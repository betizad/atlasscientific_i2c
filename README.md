# AtlasScientific
name=blink
version=1.0
author=Behzad Bayat, behzadbayat@gmail.com
maintainer=Behzad Bayat, behzadbayat@gmail.com
sentence=Library to communicate with OEM boards over I2C on Arduinos
paragraph=Library to communicate with OEM boards over I2C on Arduinos
category=Sensors
url=
architectures=*
includes=AtlasScientific.h