/*
  AtlasScientific.h - Library for communicating with AtlasScientific oem boards
  DO, PH, EC, ORP, RTD.
  Created by Behzad Bayat, July 5, 2018.
  Released rights??
*/
#include "AtlasScientific.h"

AtlasScientific::AtlasScientific(OEM_ADDR sensor_addr){
  _sensor_addr=(uint8_t) sensor_addr;
//  init();
}

uint8_t AtlasScientific::read_i2c_8(uint8_t reg)
{
  uint8_t data=0;
  Wire.beginTransmission(_sensor_addr);
  Wire.write(reg);
  Wire.endTransmission();
  Wire.requestFrom(_sensor_addr, (uint8_t) 1);
  data=Wire.read();
  Wire.endTransmission();
  return data;
}
uint16_t AtlasScientific::read_i2c_16(uint8_t reg)
{
  uint8_t n=2;
  uint16_t data=0;
  Wire.beginTransmission(_sensor_addr);
  Wire.write(reg);
  Wire.endTransmission();
  Wire.requestFrom(_sensor_addr,n);
  for (uint8_t i=0;i<n;i++){
    data=(data<<8) + Wire.read();
  }
  Wire.endTransmission();
  return data;
}

uint32_t AtlasScientific::read_i2c_32(uint8_t reg)
{
  uint8_t n=4;
  uint32_t data=0;
  Wire.beginTransmission(_sensor_addr);
  Wire.write(reg);
  Wire.endTransmission();
  Wire.requestFrom(_sensor_addr,n);
  for (uint8_t i=0;i<n;i++){
    data=(data<<8) + Wire.read();
  }
  Wire.endTransmission();
  return data;
}

void AtlasScientific::write_i2c_8(uint8_t reg, uint8_t data)
{
  Wire.beginTransmission(_sensor_addr);
  Wire.write(reg);
  Wire.write(data);
  Wire.endTransmission();
}

void AtlasScientific::write_i2c_16(uint8_t reg, uint16_t data)
{
  uint8_t n=2;
  Wire.beginTransmission(_sensor_addr);
  Wire.write(reg);
  for (uint8_t i=0;i<n;i++){
    uint8_t data_byte=(uint8_t) (data>>(8*(n-1-i)));
    Wire.write(data_byte);
  }
  Wire.endTransmission();
}

void AtlasScientific::write_i2c_32(uint8_t reg, uint32_t data)
{
  uint8_t n=4;
  Wire.beginTransmission(_sensor_addr);
  Wire.write(reg);
  for (uint8_t i=0;i<n;i++){
    uint8_t data_byte=(uint8_t) (data>>(8*(n-1-i)));
    Wire.write(data_byte);
  }
  Wire.endTransmission();
}

bool AtlasScientific::init(){
  uint32_t data1=read_i2c_32(device_type_register);
  device_type=(uint8_t)(data1>>24);
  version_number=(uint8_t) (data1>>16);
  address_lock_unlock=(uint8_t) (data1>>8);
  address=(uint8_t) (data1);
  uint32_t data2=read_i2c_32(interrupt_control_register);
  interrupt_control=(uint8_t)(data2>>24);
  led_control=(uint8_t) (data2>>16);
  active_hibernate=(uint8_t) (data2>>8);
  new_reading_available=(uint8_t) (data2);
  return true;
}

uint8_t AtlasScientific::get_device_type(){
  return device_type;
}

uint8_t AtlasScientific::get_version_number(){
  return version_number;
}

uint8_t AtlasScientific::get_address_lock_unlock(){
  return address_lock_unlock;
}

uint8_t AtlasScientific::get_address(){
  return address;
}

uint8_t AtlasScientific::get_interrupt_control(){
  return interrupt_control;
}

uint8_t AtlasScientific::get_led_control(){
  return led_control;
}

uint8_t AtlasScientific::get_active_hibernate(){
  return active_hibernate;
}

uint8_t AtlasScientific::get_new_reading_available(){
  return new_reading_available;
}

void  AtlasScientific::set_address(uint8_t new_address){
  write_i2c_8(address_lock_unlock_register, 0x55);//send 0x55,0xAA to unlock the address regsiter
  write_i2c_8(address_lock_unlock_register, 0xAA);
  write_i2c_8(address_register, new_address);     //write the new address
  write_i2c_8(address_lock_unlock_register, 0x05);//lock the address regsiter
}
void AtlasScientific::set_interrupt_control(INTERRUPT_MODE data){
  write_i2c_8(interrupt_control_register, (uint8_t) data);
}

void AtlasScientific::set_led_control(LED_MODE data){
  write_i2c_8(led_control_register, (uint8_t) data);
}

void AtlasScientific::set_active_hibernate(ACT_HIB_MODE data){
  write_i2c_8(active_hibernate_register, (uint8_t) data);
}

void AtlasScientific::set_new_reading_available(READING_AVAILABLE data){
  write_i2c_8(new_reading_available_register, (uint8_t) data);
}

uint8_t AtlasScientific::calibrate(float value=0, uint8_t mode=2){
  if (device_type==OEM_TYPE_RTD)
  {
    write_i2c_32(RTD_Calibration_register_0, (uint32_t) (value*1000) );
    write_i2c_8(RTD_Calibration_request_register, mode);
    return read_i2c_8(RTD_Calibration_confirmation_register);
  }
  else if (device_type==OEM_TYPE_ORP)
  {
    write_i2c_32(ORP_Calibration_register_0, (uint32_t) (value*10) );
    write_i2c_8(ORP_Calibration_request_register, mode);
    return read_i2c_8(ORP_Calibration_confirmation_register);
  }
  else if (device_type==OEM_TYPE_PH){
    write_i2c_32(PH_Calibration_register_0, (uint32_t) (value*1000) );
    write_i2c_8(PH_Calibration_request_register, mode);
    return read_i2c_8(PH_Calibration_confirmation_register);
  }
  else if (device_type==OEM_TYPE_EC){
    write_i2c_32(EC_Calibration_register_0, (uint32_t) (value*100) );
    write_i2c_8(EC_Calibration_request_register, mode);
    return read_i2c_8(EC_Calibration_confirmation_register);
  }
  else if (device_type==OEM_TYPE_DO){
    write_i2c_8(DO_Calibration_register, mode);
    return read_i2c_8(DO_Calibration_confirmation_register);
  }
  return 0;
}

uint8_t AtlasScientific::calibration_confirmation(){
  if (device_type==OEM_TYPE_RTD)
  {
    return read_i2c_8(RTD_Calibration_confirmation_register);
  }
  else if (device_type==OEM_TYPE_ORP)
  {
    return read_i2c_8(ORP_Calibration_confirmation_register);
  }
  else if (device_type==OEM_TYPE_PH){
    return read_i2c_8(PH_Calibration_confirmation_register);
  }
  else if (device_type==OEM_TYPE_EC){
    return read_i2c_8(EC_Calibration_confirmation_register);
  }
  else if (device_type==OEM_TYPE_DO){
    return read_i2c_8(DO_Calibration_confirmation_register);
  }
  return 0;
}

uint8_t AtlasScientific::clear_calibration(){
  if (device_type==OEM_TYPE_RTD)
  {
    write_i2c_8(RTD_Calibration_request_register, CAL_CLEAR);
    return 0;
  } 
  else if (device_type==OEM_TYPE_ORP)
  {
    write_i2c_8(ORP_Calibration_request_register, CAL_CLEAR);
    return 0;
  }
  else if (device_type==OEM_TYPE_PH)
  {
    write_i2c_8(PH_Calibration_request_register, CAL_CLEAR);
    return 0;
  }
  else if (device_type==OEM_TYPE_EC)
  {
    write_i2c_8(EC_Calibration_request_register, CAL_CLEAR);
    return 0;
  }
  else if (device_type==OEM_TYPE_DO)
  {
    write_i2c_8(DO_Calibration_register, CAL_CLEAR);
    return 0;
  }
  return 0;
}

float AtlasScientific::read(){
  if (device_type==OEM_TYPE_RTD)
  {
    return ( ( (float) read_i2c_32(RTD_reading_register_0) )/1000.0 );
  }
  else if (device_type==OEM_TYPE_ORP)
  {
    return ( ( (float) read_i2c_32(ORP_reading_register_0) )/10.0 );
  }
  else if (device_type==OEM_TYPE_PH)
  {
    return ( ( (float) read_i2c_32(PH_reading_register_0) )/1000.0 );
  }
  else if (device_type==OEM_TYPE_EC)
  {
    return ( ( (float) read_i2c_32(EC_reading_register_0) )/100.0 );
  }
  return 0xFFFFFFFF;
}

float AtlasScientific::read_EC_PSS(){
  if (device_type==OEM_TYPE_EC)
  {
    return ( ( (float) read_i2c_32(EC_PSS_reading_register_0) )/100.0 );
  }
  return 0xFFFFFFFF;
}

float AtlasScientific::read_EC_TDS(){
  if (device_type==OEM_TYPE_EC)
  {
    return ( ( (float) read_i2c_32(EC_TDS_reading_register_0) )/100.0 );
  }
  return 0xFFFFFFFF;
}

float AtlasScientific::read_DO_mg_L(){
  if (device_type==OEM_TYPE_DO)
  {
    return ( ( (float) read_i2c_32(DO_in_mg_L_register_0) )/100.0 );
  }
  return 0xFFFFFFFF;
}

float AtlasScientific::read_DO_saturation(){
  if (device_type==OEM_TYPE_DO)
  {
    return ( ( (float) read_i2c_32(DO_in_saturation_register_0) )/100.0 );
  }
  return 0xFFFFFFFF;
}

uint8_t AtlasScientific::temperature_compensate(float value){
  if (device_type==OEM_TYPE_PH)
  {
    write_i2c_32(PH_Temperature_compensation_register_0,(uint32_t) (value*100));
    return 1;
  }
  else if (device_type==OEM_TYPE_EC)
  {
    write_i2c_32(EC_Temperature_compensation_register_0,(uint32_t) (value*100));
    return 1;
  }
  else if (device_type==OEM_TYPE_DO)
  {
    write_i2c_32(DO_Temperature_compensation_register_0,(uint32_t) (value*100));
    return 1;
  }
  return 0xFF;
}

float AtlasScientific::temperature_confirmed(){
  if (device_type==OEM_TYPE_PH)
  {
    return ( ((float) read_i2c_32(PH_Temperature_confirmation_register_0))/100.0);
  }
  else if (device_type==OEM_TYPE_EC)
  {
    return ( ((float) read_i2c_32(EC_Temperature_confirmation_register_0))/100.0);
  }
  else if (device_type==OEM_TYPE_DO)
  {
    return ( ((float) read_i2c_32(DO_Temperature_confirmation_register_0))/100.0);
  }
  return 0xFFFFFFFF;
}

uint8_t AtlasScientific::salinity_compensate(float value){
  if (device_type==OEM_TYPE_DO)
  {
    write_i2c_32(DO_Salinity_compensation_register_0,(uint32_t) (value*100));
    return 1;
  }
  return 0xFF;
}

float AtlasScientific::salinity_confirmed(){
  if (device_type==OEM_TYPE_DO)
  {
    return ( ((float) read_i2c_32(DO_Salinity_confirmation_register_0))/100.0);
  }
  return 0xFFFFFFFF;
}

uint8_t AtlasScientific::pressure_compensate(float value){
  if (device_type==OEM_TYPE_DO)
  {
    write_i2c_32(DO_Pressure_compensation_register_0,(uint32_t) (value*100));
    return 1;
  }
  return 0xFF;
}

float AtlasScientific::pressure_confirmed(){
  if (device_type==OEM_TYPE_DO)
  {
    return ( ((float) read_i2c_32(DO_Pressure_confirmation_register_0))/100.0);
  }
  return 0xFFFFFFFF;
}

uint8_t AtlasScientific::set_EC_probe_type(float value){
  if (device_type==OEM_TYPE_EC)
  {
    write_i2c_16(EC_Set_probe_type_register_0,(uint16_t) (value*100));
    return 1;
  }
  return 0xFF;
}
