/*
  AtlasScientific.h - Library for communicating with AtlasScientific oem boards
  DO, PH, EC, ORP, RTD.
  Created by Behzad Bayat, July 5, 2018.
  Released rights??
*/
#ifndef ATLASSCIENTIFIC_H
#define ATLASSCIENTIFIC_H

// #include "Arduino.h"
#include <Arduino.h>
#include <Wire.h>
#include <stdint.h>

#define		device_type_register                        0x00
#define   firmware_version_register                   0x01
#define   address_lock_unlock_register                0x02
#define   address_register                            0x03
#define   interrupt_control_register                  0x04
#define   led_control_register                        0x05
#define   active_hibernate_register                   0x06
#define   new_reading_available_register              0x07

#define		EC_Set_probe_type_register_0				       	0x08
#define		EC_Set_probe_type_register_1				       	0x09
#define		EC_Calibration_register_0						        0x0A
#define		EC_Calibration_register_1						        0x0B
#define		EC_Calibration_register_2						        0x0C
#define		EC_Calibration_register_3						        0x0D
#define		EC_Calibration_request_register					    0x0E
#define		EC_Calibration_confirmation_register			  0x0F
#define		EC_Temperature_compensation_register_0			0x10
#define		EC_Temperature_compensation_register_1			0x11
#define		EC_Temperature_compensation_register_2			0x12
#define		EC_Temperature_compensation_register_3			0x13
#define		EC_Temperature_confirmation_register_0			0x14
#define		EC_Temperature_confirmation_register_1			0x15
#define		EC_Temperature_confirmation_register_2			0x16
#define		EC_Temperature_confirmation_register_3			0x17
#define		EC_reading_register_0							          0x18
#define		EC_reading_register_1							          0x19
#define		EC_reading_register_2							          0x1A
#define		EC_reading_register_3							          0x1B
#define		EC_TDS_reading_register_0						        0x1C
#define		EC_TDS_reading_register_1						        0x1D
#define		EC_TDS_reading_register_2						        0x1E
#define		EC_TDS_reading_register_3						        0x1F
#define		EC_PSS_reading_register_0						        0x20
#define		EC_PSS_reading_register_1						        0x21
#define		EC_PSS_reading_register_2						        0x22
#define		EC_PSS_reading_register_3						        0x23

#define		ORP_Calibration_register_0						      0x08
#define		ORP_Calibration_register_1						      0x09
#define		ORP_Calibration_register_2						      0x0A
#define		ORP_Calibration_register_3						      0x0B
#define		ORP_Calibration_request_register				    0x0C
#define		ORP_Calibration_confirmation_register			  0x0D
#define		ORP_reading_register_0							        0x0E
#define		ORP_reading_register_1							        0x0F
#define		ORP_reading_register_2							        0x10
#define		ORP_reading_register_3							        0x11

#define		DO_Calibration_register							        0x08
#define		DO_Calibration_confirmation_register			  0x09
#define		DO_Salinity_compensation_register_0				  0x0A
#define		DO_Salinity_compensation_register_1				  0x0B
#define		DO_Salinity_compensation_register_2				  0x0C
#define		DO_Salinity_compensation_register_3				  0x0D
#define		DO_Pressure_compensation_register_0				  0x0E
#define		DO_Pressure_compensation_register_1				  0x0F
#define		DO_Pressure_compensation_register_2				  0x10
#define		DO_Pressure_compensation_register_3				  0x11
#define		DO_Temperature_compensation_register_0			0x12
#define		DO_Temperature_compensation_register_1			0x13
#define		DO_Temperature_compensation_register_2			0x14
#define		DO_Temperature_compensation_register_3			0x15
#define		DO_Salinity_confirmation_register_0				  0x16
#define		DO_Salinity_confirmation_register_1				  0x17
#define		DO_Salinity_confirmation_register_2				  0x18
#define		DO_Salinity_confirmation_register_3				  0x19
#define		DO_Pressure_confirmation_register_0				  0x1A
#define		DO_Pressure_confirmation_register_1				  0x1B
#define		DO_Pressure_confirmation_register_2				  0x1C
#define		DO_Pressure_confirmation_register_3				  0x1D
#define		DO_Temperature_confirmation_register_0			0x1E
#define		DO_Temperature_confirmation_register_1			0x1F
#define		DO_Temperature_confirmation_register_2			0x20
#define		DO_Temperature_confirmation_register_3			0x21
#define		DO_in_mg_L_register_0							          0x22
#define		DO_in_mg_L_register_1							          0x23
#define		DO_in_mg_L_register_2							          0x24
#define		DO_in_mg_L_register_3							          0x25
#define		DO_in_saturation_register_0						      0x26
#define		DO_in_saturation_register_1						      0x27
#define		DO_in_saturation_register_2						      0x28
#define		DO_in_saturation_register_3						      0x29


#define		PH_Calibration_register_0						      0x08
#define		PH_Calibration_register_1						      0x09
#define		PH_Calibration_register_2						      0x0A
#define		PH_Calibration_register_3						      0x0B
#define		PH_Calibration_request_register					    0x0C
#define		PH_Calibration_confirmation_register			  0x0D
#define		PH_Temperature_compensation_register_0			0x0E
#define		PH_Temperature_compensation_register_1			0x0F
#define		PH_Temperature_compensation_register_2			0x10
#define		PH_Temperature_compensation_register_3			0x11
#define		PH_Temperature_confirmation_register_0			0x12
#define		PH_Temperature_confirmation_register_1			0x13
#define		PH_Temperature_confirmation_register_2			0x14
#define		PH_Temperature_confirmation_register_3			0x15
#define		PH_reading_register_0							          0x16
#define		PH_reading_register_1							          0x17
#define		PH_reading_register_2							          0x18
#define		PH_reading_register_3							          0x19

#define		RTD_Calibration_register_0						      0x08
#define		RTD_Calibration_register_1						      0x09
#define		RTD_Calibration_register_2						      0x0A
#define		RTD_Calibration_register_3						      0x0B
#define		RTD_Calibration_request_register				    0x0C
#define		RTD_Calibration_confirmation_register			  0x0D
#define		RTD_reading_register_0							        0x0E
#define		RTD_reading_register_1							        0x0F
#define		RTD_reading_register_2							        0x10
#define		RTD_reading_register_3							        0x11

enum OEM_ADDR{      
	OEM_ADDR_EC=0x64, 
	OEM_ADDR_PH, 
	OEM_ADDR_ORP,
	OEM_ADDR_DO,
	OEM_ADDR_RTD
};

enum OEM_TYPE{      
	OEM_TYPE_PH=1, 
	OEM_TYPE_ORP=2,
	OEM_TYPE_DO=3,
  OEM_TYPE_EC=4, 
	OEM_TYPE_RTD=5
};

enum INTERRUPT_MODE{      
  INTERRUPT_DISABLED=0, 
  INTERRUPT_HIGH=2, 
  INTERRUPT_LOW=4,
  INTERRUPT_INVERTED=8
};

enum LED_MODE{      
  LED_OFF=0,
  LED_ON=1
};

enum ACT_HIB_MODE{      
  HIBERNATE=0,
  ACTIVE=1
};

enum READING_AVAILABLE{      
  NO_DATA=0,
  NEW_DATA=1
};

enum CAL_MODE{
  CAL_CLEAR=1,
  CAL_SINGLE,

  CAL_PH_LOW=2,
  CAL_PH_MID,
  CAL_PH_HIGH,
  
  CAL_EC_DRY=2,
  CAL_EC_SINGLE,
  CAL_EC_DUAL_LOW,
  CAL_EC_DUAL_HIGH,
  
  CAL_DO_ATM=2,
  CAL_DO_ZERO
};



class AtlasScientific
{
  public:
    AtlasScientific(OEM_ADDR sensor_addr);
    bool init();
    const char sensor_name[6][4] = {"P", "PH", "ORP", "DO", "EC","RTD"};
    uint8_t interrupt_pin_status=0xFF;
    uint8_t interrupt_pin_status_old=0xFF;
    uint8_t INTERRUPT_PIN=0xFF;
    uint8_t get_device_type();
    uint8_t get_version_number();
    uint8_t get_address_lock_unlock();
    uint8_t get_address();
    uint8_t get_interrupt_control();
    uint8_t get_led_control();
    uint8_t get_active_hibernate();
    uint8_t get_new_reading_available();
    void    set_address(uint8_t new_address);
    void    set_interrupt_control(INTERRUPT_MODE data);
    void    set_led_control(LED_MODE data);
    void    set_active_hibernate(ACT_HIB_MODE data);
    void    set_new_reading_available(READING_AVAILABLE data);
    uint8_t calibrate(float value, uint8_t mode);
    uint8_t calibration_confirmation();
    uint8_t clear_calibration();
    uint8_t temperature_compensate(float value);
    float   temperature_confirmed();
    uint8_t salinity_compensate(float value);
    float   salinity_confirmed();
    uint8_t pressure_compensate(float value);
    float   pressure_confirmed();
    float   read();
    float   read_EC_TDS();
    float   read_EC_PSS();
    float   read_DO_mg_L();
    float   read_DO_saturation();
    uint8_t set_EC_probe_type(float value);
  private:
    uint8_t read_i2c_8(uint8_t reg);
    uint16_t read_i2c_16(uint8_t reg);
    uint32_t read_i2c_32(uint8_t reg);
    void write_i2c_8(uint8_t reg, uint8_t data);
    void write_i2c_16(uint8_t reg, uint16_t data);
    void write_i2c_32(uint8_t reg, uint32_t data);
    uint8_t _sensor_addr;
    uint8_t device_type=0xFF;
    uint8_t version_number=0xFF;
    uint8_t address_lock_unlock=0xFF;
    uint8_t address=0xFF;
    uint8_t interrupt_control=0xFF;
    uint8_t led_control=0xFF;
    uint8_t active_hibernate=0xFF;
    uint8_t new_reading_available=0xFF;
};

#endif
